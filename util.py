from functools import reduce
import operator

class FromAttrs(type):
    @classmethod
    def __prepare__(metacls, name, bases, attrs=None):
        if not attrs:
            return {}
        def property_(attr):
            def getter(self):
                return getattr(self, '_' + attr)
            def setter(self, value):
                setattr(self, '_' + attr, value)
            return property(getter, setter)
        return {attr: property_(attr) for attr in attrs}

    def __new__(cls, name, bases, classdict, attrs=None):
        class Super:
            def __init__(self, **kwargs):
                for attr in attrs:
                    setattr(self, '_' + attr, kwargs.get(attr)) 
        return type.__new__(cls, name, bases + (Super,), classdict)

    def __init__(cls, *args, attrs=None):
        return type.__init__(cls, *args)


def as_bytes(number, num_bytes):
    "Pack int into bytes so that higher bytes to go first."
    result = bytearray()
    for i in reversed(range(num_bytes)):
        result += bytearray(
            ((number & (0xFF << 8*i)) >> 8*i,))
    return result

def from_bytes(bytes_):
    "Get int from bytes, assume higher bytes go first."
    return reduce(operator.add,
        [byte << 8*i for i, byte in enumerate(reversed(bytes_))])