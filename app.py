import operator
import time
from functools import reduce
from collections import deque

from tornado.ioloop import IOLoop
from tornado.tcpserver import TCPServer
from tornado import gen

from util import FromAttrs, as_bytes, from_bytes


class Message(metaclass=FromAttrs,
              attrs="header no source_name status numfields data cksum".split()):

    HEADER = bytearray((0x01,))
    
    # statuses
    IDLE, ACTIVE, RECHARGE = [bytearray((v,)) for v in range(1,4)]
    
    @status.setter
    def status(self, value):
        assert value in (self.IDLE, self.ACTIVE, self.RECHARGE)
        self._status = value
    
    DATA_CHUNK_LEN = 12
    DATA_KEY_LEN = 8
    
    @data.setter
    def data(self, value):
        DATA_CHUNK_LEN = self.DATA_CHUNK_LEN
        DATA_KEY_LEN = self.DATA_KEY_LEN
        assert len(value) == from_bytes(self.numfields) * DATA_CHUNK_LEN
        self.raw_data = value
        chunks = [bytearray(tup) for tup in zip(*[iter(value)] * DATA_CHUNK_LEN)]
        self._data = tuple((chunk[:DATA_KEY_LEN], chunk[DATA_KEY_LEN:])
                           for chunk in chunks)

    @property
    def raw(self, cksum_included=True):
        "Restore message to it's byte form."
        parts = [(attr, getattr(self, attr)) for attr in
                 "header no source_name status numfields raw_data".split()]
        concat = bytearray()
        for name, part in parts:
            try:
                concat += part
            except TypeError:
                raise Exception("%s is not bytes: %s" % (name, type(part)))
        # if self.cksum is None, do not add checksum to result
        cksum = cksum_included and self.cksum or bytearray()
        return concat + cksum

    @cksum.setter
    def cksum(self, value):
        computed = reduce(operator.xor, self.raw, 0)
        assert value == bytearray((computed,))
        self._cksum = value
    
    def construct_response(self):
        header = bytearray((0x11,)) if self.cksum else bytearray((0x12,))
        no = self.no or bytearray((0x00, 0x00))
        cksum = reduce(operator.xor, header + no, 0)
        cksum = bytearray((cksum,))
        return header + no + cksum


class MessagesStreamHandler:
    
    class MessageSource:
        def __init__(self, source_name=None, status=None, last_msg_no=None):
            self.source_name = source_name
            self._last_activity = time.time()
            self.status = status
            self.last_msg_no = last_msg_no
    
        @property
        def last_activity(self):
            # in milliseconds
            return round(1000 * time.time() - 1000 * self._last_activity
                ) if self._last_activity else None

        
    def __init__(self, stream, address, on_new_message):
        self.stream = stream
        self.address = address
        self.on_new_message = on_new_message
        self.source = None # will be figured out after the first message
        
        IOLoop.instance().add_future(self.read_forever(),
            lambda f: print("message stream read finished"))

    @gen.coroutine
    def read_forever(self):
        while True:
            yield gen.Task(self.stream.read_until, Message.HEADER)
            msg = (yield self.read_message())
            if msg:
                self.on_new_message(msg)
                self.source = self.MessageSource(msg.source_name,
                    last_msg_no=from_bytes(msg.no),
                    status={1: 'IDLE', 2: 'ACTIVE', 3: 'RECHARGE'
                            }.get(msg.status[0]))
    
    @gen.coroutine
    def read_message(self):
        read_bytes = lambda num_bytes: gen.Task(self.stream.read_bytes, num_bytes)
        msg = Message(header=Message.HEADER)
        try:
            msg.no = yield read_bytes(2)
            msg.source_name = yield read_bytes(8)
            msg.status = yield read_bytes(1)
            msg.numfields = yield read_bytes(1)
            numfields = from_bytes(msg.numfields)
            msg.data = yield read_bytes(numfields * Message.DATA_CHUNK_LEN)
            msg.cksum = yield read_bytes(1)
        except AssertionError:
            print("Invalid message")
            result = None
        else:
            result = msg
        response = msg.construct_response()
        self.stream.write(bytes(response))
        return result


class TCP_0(TCPServer):
    '''
    TCP Server that accepts connections from clients sending messages.
    '''
    
    def __init__(self, on_new_message):
        self.on_new_message = on_new_message
        self.connections = set()
        super().__init__()
    
    @property    
    def connected_clients(self):
        return filter(None, [conn.source for conn in self.connections])

    def handle_stream(self, stream, address):
        handler = MessagesStreamHandler(stream, address, self.on_new_message)
        self.connections.add(handler)
        stream.set_close_callback(
            lambda: self.connections.remove(handler))


class TCP_1(TCPServer):
    '''
    TCP Server that accepts connections from messages listeners.
    '''
    
    def __init__(self, tcp0=None):
        self.tcp0 = tcp0 # server that listens for new messages
        self.listeners = set() # streams
        super().__init__()
    
    def on_new_message(self, msg):
        for stream in self.listeners:
            def lines():
                for key, value in msg.data:
                    source_name = msg.source_name.decode('ascii')
                    value = from_bytes(value)
                    key = key.decode('ascii')
                    yield "[%(source_name)s] %(key)s | %(value)s\r\n" % locals()
            stream.write('\r\n'.join(lines()).encode('ascii'))
    
    def greet(self, stream):
        sources = list(self.tcp0.connected_clients)
        def lines():
            for source in sources:
                last_activity=source.last_activity
                source_name = source.source_name.decode('ascii')
                yield ("[%(source_name)s] %(last_msg_no)s "
                       "| %(status)s | %(last_activity)s"
                       % dict(source.__dict__, **locals()))
        greeting = "\r\n".join(lines()).encode('ascii')
        stream.write(greeting)
    
    def handle_stream(self, stream, address):
        self.listeners.add(stream)
        stream.set_close_callback(
            lambda: self.listeners.remove(stream))
        self.greet(stream)


if __name__ == '__main__':
    tcp1 = TCP_1()
    tcp1.listen(8889)
    tcp0 = TCP_0(on_new_message=tcp1.on_new_message)
    tcp0.listen(8888)
    tcp1.tcp0 = tcp0
    
    IOLoop.instance().start()
