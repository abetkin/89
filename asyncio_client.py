import asyncio

from app import Message, as_bytes, from_bytes
import operator
from functools import reduce

def new_msg():
    msg = Message(header=Message.HEADER)
    msg.no = as_bytes(1000, 2)
    msg.source_name = bytearray(b'source00')
    msg.status = Message.IDLE
    msg.numfields = as_bytes(1, 1)
    msg.data = bytearray(b'firstkey') + as_bytes(0x01FF, 4)
    msg.cksum = bytearray([reduce(operator.xor, msg.raw, 0)])
    return msg

class Client(asyncio.Protocol):

    def connection_made(self, transport):
        raw = new_msg().raw
        for i in '12345':
            transport.write(raw)

    def data_received(self, data):
        print('%s' % data)

    def connection_lost(self, exc):
        print('server closed the connection')
        asyncio.get_event_loop().stop()

loop = asyncio.get_event_loop()
coro = loop.create_connection(Client, '127.0.0.1', 8888)
loop.run_until_complete(coro)
loop.run_forever()
loop.close()